GWT-Chat
========

A simple demo application for GWT.

To run it, you just need Ant (http://ant.apache.org/bindownload.cgi) as well as the GWT SDK (https://developers.google.com/web-toolkit/download).

It uses Twitter Bootstrap (http://twitter.github.io/bootstrap/) for the fronted.

To run, simply run ant (replace /opt/gwt-sdk/ with your GWT SDK installation path):

	ant -Dgwt.sdk=/opt/gwt-sdk/ devmode

Then, click the "Launch Default Browser" button in the GUI.
