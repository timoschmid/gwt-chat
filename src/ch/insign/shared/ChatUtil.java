package ch.insign.shared;

public class ChatUtil {

    public static boolean isEmpty(String str) {
        return str == null || str.length() == 0;
    }

    public static String formatMessage(String name, String message) {
        if(message.startsWith("/me ")) {
            return name + message.substring(3);
        } else {
            return name + ": " + message;
        }
    }

}
