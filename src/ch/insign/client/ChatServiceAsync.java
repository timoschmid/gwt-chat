package ch.insign.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.ArrayList;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface ChatServiceAsync {

    void sendMessage(String message, AsyncCallback<String> callback) throws IllegalArgumentException;

    void getMessages(AsyncCallback<ArrayList<String>> callback);

}
