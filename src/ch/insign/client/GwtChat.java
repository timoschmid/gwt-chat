package ch.insign.client;

import ch.insign.shared.ChatUtil;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class GwtChat implements EntryPoint {
    /**
     * The message displayed to the user when the server cannot be reached or
     * returns an error.
     */
    private static final String SERVER_ERROR = "An error occurred while "
            + "attempting to contact the server. Please check your network "
            + "connection and try again.";

    /**
     * Create a remote service proxy to talk to the server-side Greeting service.
     */
    private final ChatServiceAsync chatService = GWT.create(ChatService.class);

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {

        final TextBox fieldName = new TextBox();
        final Label errorName = new Label();
        RootPanel.get("field-name").add(fieldName);
        RootPanel.get("error-name").add(errorName);

        final TextBox fieldMessage = new TextBox();
        final Label errorMessage = new Label();
        RootPanel.get("field-message").add(fieldMessage);
        RootPanel.get("error-message").add(errorMessage);

        final Button buttonSend = new Button("Send");
        RootPanel.get("button-send").add(buttonSend);

        final Label outputLabel = new Label();
        RootPanel.get("output").add(outputLabel);

        fieldName.setFocus(true);
        fieldName.selectAll();

        class MyHandler implements ClickHandler, KeyUpHandler {

            @Override
            public void onClick(ClickEvent clickEvent) {
                sendMessage();
            }

            @Override
            public void onKeyUp(KeyUpEvent keyUpEvent) {
                if (keyUpEvent.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    sendMessage();
                }
            }

            private void sendMessage() {
                final String name = fieldName.getText();
                final String message = fieldMessage.getText();
                boolean error = false;
                if (ChatUtil.isEmpty(name)) {
                    errorName.setText("The name cannot be empty.");
                    RootPanel.get("control-group-name").addStyleName("error");
                    error = true;
                } else {
                    RootPanel.get("control-group-name").removeStyleName("error");
                }
                if (ChatUtil.isEmpty(message)) {
                    errorMessage.setText("The message cannot be empty.");
                    RootPanel.get("control-group-message").addStyleName("error");
                    error = true;
                } else {
                    RootPanel.get("control-group-message").removeStyleName("error");
                }
                if (!error) {
                    RootPanel.get("control-group-name").removeStyleName("error");
                    final String formattedMessage = ChatUtil.formatMessage(name, message);
                    chatService.sendMessage(formattedMessage, new AsyncCallback<String>() {
                        @Override
                        public void onFailure(Throwable throwable) {
                            outputLabel.setText("FAIL: sendMessage(" + formattedMessage + "):\n" + throwable.getMessage() + "\n\nSTACK TRACE:\n" + stackTraceToString(throwable));
                        }

                        @Override
                        public void onSuccess(String s) {
                            refreshMessages(outputLabel);
                        }
                    });
                }
            }

        }

        MyHandler h = new MyHandler();

        fieldName.addKeyUpHandler(h);
        fieldMessage.addKeyUpHandler(h);
        buttonSend.addClickHandler(h);

        Timer timer = new Timer() {
            @Override
            public void run() {
                refreshMessages(outputLabel);
            }
        };
        timer.run();
        timer.scheduleRepeating(2000);
    }

    public void refreshMessages(final Label debugLabel) {
        chatService.getMessages(new AsyncCallback<ArrayList<String>>() {

            @Override
            public void onFailure(Throwable throwable) {
                debugLabel.setText("FAIL: getMessages()\n" + throwable.getMessage() + "\n\nSTACK TRACE:\n" + stackTraceToString(throwable));
            }

            @Override
            public void onSuccess(ArrayList<String> strings) {
                StringBuffer sb = new StringBuffer();
                if (strings.size() == 0) {
                    sb.append("No messages yet.");
                } else {
                    for (String message : strings) {
                        sb.append(message);
                        sb.append("\n");
                    }
                }
                debugLabel.setText(sb.toString());
            }

        });
    }

    public String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
}
