package ch.insign.server;

import ch.insign.client.ChatService;
import ch.insign.shared.ChatUtil;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

import java.util.ArrayList;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class ChatServiceImpl extends RemoteServiceServlet implements ChatService {

    private ArrayList<String> messages = new ArrayList<String>();

    public void sendMessage(String message) throws IllegalArgumentException {
        // Verify that the input is valid.
        if (!ChatUtil.isEmpty(message)) {
            messages.add(message);
        } else {
            throw new IllegalArgumentException("No message given: " + message);
        }
    }

    @Override
    public ArrayList<String> getMessages() {
        return messages;
    }
}
